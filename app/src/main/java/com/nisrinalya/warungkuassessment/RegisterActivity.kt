package com.nisrinalya.warungkuassessment

import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.card.MaterialCardView
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.database.*
import java.util.regex.Pattern

class RegisterActivity : AppCompatActivity() {
    private lateinit var btnRegistration: MaterialCardView
    
    private lateinit var inputEmailContainer: TextInputLayout
    private lateinit var inputPassContainer: TextInputLayout
    private lateinit var reinputPassContainer: TextInputLayout
    
    private lateinit var inputEmail: TextInputEditText
    private lateinit var inputPass: TextInputEditText
    private lateinit var reinputPass: TextInputEditText

    private lateinit var db: FirebaseDatabase
    private lateinit var dbRef: DatabaseReference

    private var isPassVisible = false
    private var isRepassVisible = false
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        
        btnRegistration = findViewById(R.id.btn_register)
        
        inputEmailContainer = findViewById(R.id.input_email_container)
        inputPassContainer = findViewById(R.id.input_pass_container)
        reinputPassContainer = findViewById(R.id.reinput_pass_container)

        inputEmail = findViewById(R.id.input_email)
        inputPass = findViewById(R.id.input_pass)
        reinputPass = findViewById(R.id.reinput_pass)

        db = FirebaseDatabase.getInstance()
        dbRef = db.getReference("User")

        inputPassContainer.setEndIconOnClickListener {
            if (isPassVisible) {
                inputPassContainer.endIconDrawable = ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_round_visibility_off,
                    null
                )
                
                inputPass.inputType = InputType.TYPE_CLASS_TEXT
                
                isPassVisible = false
            } else {
                inputPassContainer.endIconDrawable = ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_round_visibility,
                    null
                )

                inputPass.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                
                isPassVisible = true
            }
        }

        reinputPassContainer.setEndIconOnClickListener {
            if (isRepassVisible) {
                reinputPassContainer.endIconDrawable = ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_round_visibility_off,
                    null
                )

                reinputPass.inputType = InputType.TYPE_CLASS_TEXT

                isRepassVisible = false
            } else {
                reinputPassContainer.endIconDrawable = ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_round_visibility,
                    null
                )

                reinputPass.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

                isRepassVisible = true
            }
        }
        
        inputPass.addTextChangedListener(onPass())
        reinputPass.addTextChangedListener(onRepass())

        btnRegistration.setOnClickListener {
            if (inputPass.text?.length!! < 6 || reinputPass.text?.toString() != inputPass.text?.toString()) {
                checkPassError()
                checkRepassError()
            } else {
                dbRef.runTransaction(object : Transaction.Handler{
                    override fun doTransaction(p0: MutableData): Transaction.Result {
                        if (p0.value == null) {
                            addUser("1")
                        } else {
                            p0.getValue(Int.javaClass)
                        }

                        return Transaction.success(p0)
                    }

                    override fun onComplete(p0: DatabaseError?, p1: Boolean, p2: DataSnapshot?) {

                    }
                })
            }
        }
    }

    private fun addUser(id: String) {
        val reference = dbRef.child(id)
        if (isEmail()) {
            reference.child("email").setValue(inputEmail.text.toString())
        } else if (isPhoneNumber()) {
            reference.child("phone").setValue(inputEmail.text.toString())
        }

        reference.child("password").setValue(inputPass.text.toString())
    }

    private fun onRepass(): TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            checkRepassError()
        }

        override fun afterTextChanged(p0: Editable?) {

        }
    }

    private fun checkRepassError() {
        if (reinputPass.text?.toString() != inputPass.text?.toString()) {
            reinputPassContainer.isErrorEnabled = true
            reinputPassContainer.error = "Password tidak boleh kurang dari 6 karakter"
        } else {
            reinputPassContainer.isErrorEnabled = false
            reinputPassContainer.error = null
        }
    }

    private fun onPass(): TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            checkPassError()
        }

        override fun afterTextChanged(p0: Editable?) {
            
        }
    }

    private fun checkPassError() {
        if (inputPass.text?.length!! < 6) {
            inputPassContainer.isErrorEnabled = true
            inputPassContainer.error = "Password tidak boleh kurang dari 6 karakter"
        } else {
            inputPassContainer.isErrorEnabled = false
            inputPassContainer.error = null
        }
    }

    private fun isPhoneNumber(): Boolean {
        val phone = inputEmail.text.toString()
        val regex = "^(\\+\\d{1,3}( )?)?((\\(\\d{1,3}\\))|\\d{1,3})[- .]?\\d{3,4}[- .]?\\d{4}$"

        val pattern = Pattern.compile(regex)
        return pattern.matcher(phone).matches()
    }

    private fun isEmail(): Boolean {
        val email = inputEmail.text.toString()
        val regex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$"

        val pattern = Pattern.compile(regex)
        return pattern.matcher(email).matches()
    }
}